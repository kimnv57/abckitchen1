$(function(){
    var picker = new Pikaday(
    {
        field: document.getElementById('myCalendar'),
        firstDay: 1,
        format: 'D MMM YYYY',
        minDate: new Date('2000-01-01'),
        maxDate: new Date('2020-12-31'),
        yearRange: [2000, 2020],
        bound: false,
        showWeekNumber: true,
        onSelect: function() {
            getFuel();
        }
    });
    picker.setDate(new Date(),0);

    function getFuel() {
        $("#new").attr("href","menus/create/"+date);
    var date=moment(picker.getDate()).format("YYYY-MM-DD");
        $.ajax({
            url : link,
            type : 'GET',
            dataType:'json',
            success : function(data) {
                $(".iframe").colorbox({
                    iframe:true, width:"85%", height:"98%",
                    onClosed: function () {
                        getMenu(); 
                    }
                });
                
            },
            error : function(request,error)
            {
            }
        });
    }

    $(".iframe").colorbox({
        iframe:true, width:"85%", height:"98%",
        onClosed: function () {
            getFuel(); 
        }
    });
               
        
});

