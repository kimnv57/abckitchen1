
<nav class="my-navbar navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{!! URL::to('/') !!}">ABCkitchen</a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="{{ (Request::is('admin/dashboard') ? 'active' : '') }}">
                        <a href="{!! URL::to('admin/dashboard') !!}">
                            <i class="glyphicon glyphicon-stats"></i> Thống kê</a>
                    </li>

                   <li {{ (Request::is('admin/statistical*') ? ' class=active' : '') }}>
                        <a href="{{URL::to('admin/statisticals')}}">
                            <i class="glyphicon glyphicon-flag"></i><span
                                    class="hidden-sm text">Thống Kê Đến Ăn</span>
                        </a>
                    </li>

                    <li {{ (Request::is('admin/dishes*') ? ' class=active' : '') }}>
                        <a href="{{URL::to('admin/dishes')}}"
                                >
                            <i class="glyphicon glyphicon-cutlery"></i><span
                                    class="hidden-sm text"> Món Ăn</span>
                        </a>
                    </li>
                    <li  {{ (Request::is('admin/fueldailys*') ? ' class=active' : '') }}>
                        <a href="{{URL::to('admin/fueldailys')}}"
                                >
                            <i class="glyphicon glyphicon-shopping-cart"></i><span
                                    class="hidden-sm text"> Đi Chợ</span>
                        </a>
                    </li>
                    <li {{ (Request::is('admin/menus*') ? ' class=active' : '') }}>
                        <a href="{{URL::to('admin/menus')}}"
                                >
                            <i class="glyphicon glyphicon-list-alt"></i><span
                                    class="hidden-sm text"> Thực Đơn</span>
                        </a>
                    </li>
                    
                    
                    <li {{ (Request::is('admin/users*') ? ' class=active' : '') }} >
                        <a href="{{URL::to('admin/users')}}"
                                >
                            <i class="glyphicon glyphicon-user"></i><span
                                    class="hidden-sm text"> Người dùng</span>
                        </a>
                    </li>

                    <li {{ (Request::is('admin/news*') ? ' class=active' : '') }} >
                        <a href="{{URL::to('admin/news')}}"
                                >
                            <i class="glyphicon glyphicon-book"></i><span
                                    class="hidden-sm text"> Tin Tức</span>
                        </a>
                    </li>

                    <li  {{ (Request::is('admin/roles*') ? ' class=active' : '') }}>
                        <a href="#"
                                >
                            <i class="glyphicon glyphicon-tasks"></i><span
                                    class="hidden-sm text"> Chức vụ</span>
                        </a>
                    </li>
                                </ul>
                

                <ul class="nav navbar-nav navbar-right">
                    @if (Auth::guest())
                        <li class="{{ (Request::is('auth/login') ? 'active' : '') }}" data-toggle="modal" data-target="#myModal"><a ><i
                                    class="fa fa-sign-in"></i> Login</a></li>

                    @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-expanded="false"><i ></i> {{ Auth::user()->name }} <i
                                    class="glyphicon glyphicon-triangle-bottom"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            
                            <li>
                                <a href="{{ URL::to('users/' . Auth::user()->username) }}"><i class="fa fa-sign-out"></i> Ho So</a>
                            </li>
                             <li role="presentation" class="divider"></li>
                            @if(Auth::check())
                                @if(Auth::user()->hasRole('admin'))
                                    <li>
                                        <a href="{!! URL::to('home') !!}"><i class="fa fa-tachometer"></i> Trang người dùng</a>
                                    </li>
                                    <li role="presentation" class="divider"></li>
                                @endif
                                
                            @endif
                            <li>
                                <a href="{!! URL::to('auth/logout') !!}"><i class="fa fa-sign-out"></i> Đăng xuất</a>
                            </li>
                        </ul>
                    </li>
                @endif
                </ul>
            </div>
        </div>
    </nav>