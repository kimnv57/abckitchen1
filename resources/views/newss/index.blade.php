@extends('frontend')
@section('styles')
@stop

@section('content')

<div id="header">
	<img src="{{URL::to('images/news.png')}}" class="newscaptain">
	<div class="capstain">
		<h1>Tin tức - Sự kiện</h1>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-md-8" id="bodyNews">
			<h2>{{$news->title}}</h2> <br> <br>
+			<img src="{{URL::to('appfiles/newsimages/'. $news->image)}}" class="newscaptain1">
			<hr>
			<p>
				{{$news->content}}
			</p>
			<p class="pull-right">{{$news->created_at}}</p>
		</div>
		<div class="col-md-4">
			@include('layouts.layoutright-news')
		</div>
	</div>
</div>
 @stop

 @section('scripts')

@stop