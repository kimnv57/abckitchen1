<!DOCTYPE html>
<html lang="en">
     <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
     <title>@section('title') Abc kitchen @show</title>

     @yield('styles')
     <link rel="icon" href="images/favicon.ico">
     <link rel="shortcut icon" href="images/favicon.ico" />
     <link rel="stylesheet" type="text/css" href="{{URL::to('css/bootstrap.min.css')}}">
     <link rel="stylesheet" type="text/css" href="{{URL::to('css/backendstyle.css')}}">
     <link rel="stylesheet" type="text/css" href="{{URL::to('css/footer.css')}}">
     <link rel="stylesheet" type="text/css" href="{{URL::to('css/mystyle.css')}}">
     <!-- <link href="{{ asset('/css/all.css') }}" rel="stylesheet"> -->
     
     </head>
<body>
  @include('partials.dashboard.nav')
  <div class="container">
    @yield('content')
  </div>
</body>
<div class="my_footer">
      @include('partials.footer-design')
</div>
<script src="{{ URL::to('/js/jquery-1.9.1.min.js') }}"></script>
<script src="{{ URL::to('/js/bootstrap.min.js') }}"></script>
@yield('scripts')

</html>