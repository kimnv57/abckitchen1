<?php

use App\Fuel;
use Illuminate\Database\Seeder;

class FuelsTableSeeder extends Seeder {

    public function run()
    {
        DB::table('fuels')->delete();
    }

}
