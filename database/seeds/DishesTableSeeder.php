<?php

use App\Dish;
use Illuminate\Database\Seeder;

class DishesTableSeeder extends Seeder {

    public function run()
    {
        DB::table('dishes')->delete();

        $dish = new Dish;
        $dish->name = 'Bò sốt vang';
        $dish->image = 'boSotVang.jpg';
        $dish->description = 'description';
        $dish->price = '10000';
        $dish->type = '2';
        $dish->save();


        $dish = new Dish;
        $dish->name = 'Bò xào cần tỏi ';
        $dish->image = 'BoXaoCanToi.jpg';
        $dish->description = 'description';
        $dish->price = '20000';
        $dish->type = '2';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Cá bống chiên giòn';
        $dish->image = 'CaBongChienGion.jpeg';
        $dish->description = 'description';
        $dish->price = '15000';
        $dish->type = '2';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Cá khô xào chua ngọt';
        $dish->image = 'CaKhoXaoChuaNgot.jpg';
        $dish->description = 'description';
        $dish->price = '10000';
        $dish->type = '2';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Cá trôi kho tộ';
        $dish->image = 'Catroikhoto.jpg';
        $dish->description = 'description';
        $dish->price = '15000';
        $dish->type = '2';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Chả lá nốt';
        $dish->image = 'Chalanot.jpg';
        $dish->description = 'description';
        $dish->price = 18000;
        $dish->type = '2';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Gà xả ớt';
        $dish->image = 'gaxaot.jpg';
        $dish->description = 'description';
        $dish->price = 18000;
        $dish->type = '2';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Gà om nấm';
        $dish->image = 'gaomnam.png';
        $dish->description = 'description';
        $dish->price = 10000;
        $dish->type = '2';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Nạc vai rim tiêu';
        $dish->image = 'nacvairimtieu.jpg';
        $dish->description = 'description';
        $dish->price = 8000;
        $dish->type = '2';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Ốc xào xả ớt';
        $dish->image = 'ocxaoxaot.jpg';
        $dish->description = 'description';
        $dish->price = 5000;
        $dish->type = '1';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Sường rim tiêu';
        $dish->image = 'suonrimtieu.jpg';
        $dish->description = 'description';
        $dish->price = 10000;
        $dish->type = '2';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Sườn sốt chua ngọt';
        $dish->image = 'suonsotchuangot.jpg';
        $dish->description = 'description';
        $dish->price = 10000;
        $dish->type = '2';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Thăn lợn chiên xù';
        $dish->image = 'thanlonchienxu.jpg';
        $dish->description = 'description';
        $dish->price = 8000;
        $dish->type = '2';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Thịt ba chỉ quay';
        $dish->image = 'thitbachiquay.jpg';
        $dish->description = 'description';
        $dish->price = 10000;
        $dish->type = '2';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Thịt kho cùi dừa';
        $dish->image = 'thitkhocuidua.jpeg';
        $dish->description = 'description';
        $dish->price = 10000;
        $dish->type = '2';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Thịt kho tàu';
        $dish->image = 'thitkhotau.jpg';
        $dish->description = 'description';
        $dish->price = 10000;
        $dish->type = '2';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Thịt kho trứng cút';
        $dish->image = 'thitkhotrungcut.jpg';
        $dish->description = 'description';
        $dish->price = 10000;
        $dish->type = '2';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Thịt viên sốt';
        $dish->image = 'thitviensot.jpg';
        $dish->description = 'description';
        $dish->price = 8000;
        $dish->type = '2';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Thịt xiên lá móc mật';
        $dish->image = 'thitxienlamocmat.jpg';
        $dish->description = 'description';
        $dish->price = 10000;
        $dish->type = '2';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Tôm rang lá chanh';
        $dish->image = 'tomranglachanh.jpeg';
        $dish->description = 'description';
        $dish->price = 10000;
        $dish->type = '2';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Vịt om xấu';
        $dish->image = 'vitomxau.jpg';
        $dish->description = 'description';
        $dish->price = 10000;
        $dish->type = '3';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Cơm gạo Thái Bình';
        $dish->image = 'com.jpg';
        $dish->description = 'description';
        $dish->price = 4000;
        $dish->type = '2';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Khoai tây xào';
        $dish->image = 'khoaitayxao.jpg';
        $dish->description = 'description';
        $dish->price = 5000;
        $dish->type = '2';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Cải ngọt xào tỏi';
        $dish->image = 'caingotxao.jpg';
        $dish->description = 'description';
        $dish->price = 3000;
        $dish->type = '2';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Trứng tráng hành';
        $dish->image = 'trungtranghanh.jpg';
        $dish->description = 'description';
        $dish->price = 4000;
        $dish->type = '2';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Canh rau củ';
        $dish->image = 'canhraucu.jpg';
        $dish->description = 'description';
        $dish->price = 7000;
        $dish->type = '3';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Đậu phụ sốt cà chua';
        $dish->image = 'dausotcachua.jpg';
        $dish->description = 'description';
        $dish->price = 4000;
        $dish->type = '2';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Rau muống luộc';
        $dish->image = 'raumuongluoc.jpg';
        $dish->description = 'description';
        $dish->price = 2000;
        $dish->type = '2';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Giò lụa';
        $dish->image = 'giolua.jpg';
        $dish->description = 'description';
        $dish->price = 7000;
        $dish->type = '1';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Thịt rang hành';
        $dish->image = 'thitranghanh.jpg';
        $dish->description = 'description';
        $dish->price = 7000;
        $dish->type = '2';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Rau muống xào';
        $dish->image = 'raumuongxao.jpg';
        $dish->description = 'description';
        $dish->price = 4000;
        $dish->type = '2';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Canh xuong bí xanh';
        $dish->image = 'canhxuongbixanh.jpg';
        $dish->description = 'description';
        $dish->price = 7000;
        $dish->type = '3';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Canh rau dền';
        $dish->image = 'canhrauden.jpg';
        $dish->description = 'description';
        $dish->price = 4000;
        $dish->type = '3';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Bắp cải muối';
        $dish->image = 'bapcaimuoi.jpg';
        $dish->description = 'description';
        $dish->price = 2000;
        $dish->type = '2';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Canh cua mồng tơi';
        $dish->image = 'canhcuamongtoi.jpg';
        $dish->description = 'description';
        $dish->price = 5000;
        $dish->type = '3';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Tôm rim nước cốt dừa';
        $dish->image = 'tomrimnuoccotdua.jpg';
        $dish->description = 'description';
        $dish->price = 5000;
        $dish->type = '2';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Canh mướp đắng';
        $dish->image = 'canhmuopdang.jpg';
        $dish->description = 'description';
        $dish->price = 5000;
        $dish->type = '3';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Cà pháo muối';
        $dish->image = 'food.png';
        $dish->description = 'description';
        $dish->price = 2000;
        $dish->type = '2';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Dưa cải muối chua';
        $dish->image = 'duacaimuoichua.jpg';
        $dish->description = 'description';
        $dish->price = 2000;
        $dish->type = '2';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Cá bống kho tiêu';
        $dish->image = 'cabongkhotieu.jpg';
        $dish->description = 'description';
        $dish->price = 5000;
        $dish->type = '2';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Canh rau má';
        $dish->image = 'canhrauma.jpg';
        $dish->description = 'description';
        $dish->price = 5000;
        $dish->type = '3';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Canh bí đao';
        $dish->image = 'food.png';
        $dish->description = 'description';
        $dish->price = 5000;
        $dish->type = '3';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Canh cải bó xôi nấu thịt';
        $dish->image = 'food.png';
        $dish->description = 'description';
        $dish->price = 7000;
        $dish->type = '3';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Canh mướp đắng nhồi thịt';
        $dish->image = 'canhmuopdangnhoithit.jpg';
        $dish->description = 'description';
        $dish->price = 7000;
        $dish->type = '3';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Canh rau ngót';
        $dish->image = 'food.png';
        $dish->description = 'description';
        $dish->price = 5000;
        $dish->type = '3';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Canh sau hào';
        $dish->image = 'food.png';
        $dish->description = 'description';
        $dish->price = 4000;
        $dish->type = '3';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Dưa chuột';
        $dish->image = 'food.png';
        $dish->description = 'description';
        $dish->price = 5000;
        $dish->type = '1';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Ếch xào măng';
        $dish->image = 'echxaomang.jpg';
        $dish->description = 'description';
        $dish->price = 10000;
        $dish->type = '2';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Mận dầm';
        $dish->image = 'food.png';  
        $dish->description = 'description';
        $dish->price = 5000;
        $dish->type = '1';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Khoai tây bọc xúc xích chiên';
        $dish->image = 'food.png';
        $dish->description = 'description';
        $dish->price = 10000;
        $dish->type = '1';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Kem rán chiên giòn';
        $dish->image = 'food.png';
        $dish->description = 'description';
        $dish->price = 7000;
        $dish->type = '1';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Thạch ba lớp';
        $dish->image = 'food.png';
        $dish->description = 'description';
        $dish->price = 8000;
        $dish->type = '1';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Lòng vịt xào su hào';
        $dish->image = 'food.png';
        $dish->description = 'description';
        $dish->price = 7000;
        $dish->type = '2';
        $dish->save();


        $dish = new Dish;
        $dish->name = 'Mực hấp bia xả';
        $dish->image = 'food.png';
        $dish->description = 'description';
        $dish->price = 10000;
        $dish->type = '2';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Mực xào chua ngọt';
        $dish->image = 'food.png';
        $dish->description = 'description';
        $dish->price = 10000;
        $dish->type = '2';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Ngao xào cải bó xôi';
        $dish->image = 'food.png';
        $dish->description = 'description';
        $dish->price = 5000;
        $dish->type = '2';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Râu mực chiên giòn';
        $dish->image = 'food.png';
        $dish->description = 'description';
        $dish->price = 7000;
        $dish->type = '2';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Soup hải sản';
        $dish->image = 'food.png';
        $dish->description = 'description';
        $dish->price = 8000;
        $dish->type = '3';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Thịt chân giò bó';
        $dish->image = 'food.png';
        $dish->description = 'description';
        $dish->price = 7000;
        $dish->type = '2';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Thịt sụn sốt me';
        $dish->image = 'food.png';
        $dish->description = 'description';
        $dish->price = 6000;
        $dish->type = '2';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Tôm rang muối';
        $dish->image = 'food.png';
        $dish->description = 'description';
        $dish->price = 5000;
        $dish->type = '2';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Tôm xào lặc lày';
        $dish->image = 'food.png';
        $dish->description = 'description';
        $dish->price = 7000;
        $dish->type = '2';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Thịt xiên hoa quả';
        $dish->image = 'food.png';
        $dish->description = 'description';
        $dish->price = 5000;
        $dish->type = '1';
        $dish->save();
    }

}
