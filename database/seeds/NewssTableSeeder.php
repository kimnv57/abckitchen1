<?php

use App\News;
use Illuminate\Database\Seeder;

class NewssTableSeeder extends Seeder {

    public function run()
    {
        DB::table('newss')->delete();

        $news = new News;
        $news->title = 'Đầu bếp Tuấn Hải';
        $news->image = 'daubepTuanhai.jpg';
        $news->content = 'Trong giới mộ điệu, MasterChef Tuấn Hải luôn được vị nể bởi sự am hiểu sâu sắc về văn hóa ẩm thực, sự nghiêm túc trong công việc.. và niềm đam mê sáng tạo ẩm thực mãnh liệt. Với cương vị là một trong những nguời đầu tiên khởi xướng nghề food stylist ở Việt Nam, vị giám khảo khó tính của MasterChef Việt với hơn 20 năm trải nghiệm trong lĩnh vực ẩm thực chuyên nghiệp luôn kĩ luỡng và tinh tế trong từng tác phẩm của mình. “Đằng sau tất cả những món ăn là những cảm nhận, niềm đam mê, và sự nhiệt huyết của một đầu bếp. Một đầu bếp giỏi là người luôn biết cách đem đến những thông điệp cảm xúc cho người thưởng thức.”  – Vua Đầu Bếp Tuấn Hải
        Phương châm “Nếu bạn không hài lòng, đừng thỏa hiệp” như lời cam kết về chất lượng từ MasterChef Tuấn Hải với khách hàng.';
        $news->type = '1';
        $news->save();

        $news = new News;
        $news->title = 'Miễn phí trà đá';
        $news->image = 'mienphitrada.jpg';
        $news->content = 'Nhà hàng miễn phí trà đá cho tất cả công nhân.
        Tại điểm “trà đá miễn phí” trên đường Giải Phóng, đoạn gần với ngã 3 Giải Phóng - Trương Định (Hoàng Mai - Hà Nội), khá nhiều người đi đường dừng xe ghé vào uống cốc trà đá mát lạnh, giúp đẩy lui cái nắng nóng gay gắt của thời tiết Hà Nội.
        Ông Đinh Văn Khương (45 tuổi, quê Nam Định) làm nghề xe ôm gần khu vực này cho biết: “Bình trà đá miễn phí này tốt quá chứ. Chúng tôi khát là cứ ra đây rót nước uống, đỡ tiền đi khá nhiều. Trước đây hàng ngày tôi cũng phải mất tầm 10-15.000 đồng tiền trà đá. Xin cảm ơn người chủ bình trà tốt bụng!”.

        Trao đổi với PV Dân trí, anh Trần Nam Anh (28 tuổi, quê Tiên Lữ - Hưng Yên) - chủ nhân của bình trà đá miễn phí trên chia sẻ: “Tôi thuê cửa hàng gần đây để làm đồ sắt, tôi nghĩ mua hè nóng nực nhiều người rất có nhu cầu uống nước, nhưng người lao động nghèo không phải ai cũng dám vào quán uống. Tôi nảy sinh ý định sắm 1 bình trà đá vừa để phục vụ anh em làm việc trong cửa hàng, vừa để cho người đi đường uống miễn phí. Chi phí vật chất không đáng bao nhiêu, bình trà thì tôi có sẵn, chỉ mua thêm mấy cái cốc và làm cái vòi cho thuận tiện lấy nước. Trà tôi pha là trà mạn, ngày nóng nực như này cũng hết vài 3 bình. Tôi mới thực hiện việc này được 3 ngày”.

        Anh Trần Nam Anh cho biết thêm, ban đầu nhiều người còn e ngại không uống, anh thường phải chủ động ra mời họ uống. Dần dần nhiều người quen và qua đây uống thường xuyên. Anh Nam Anh hy vọng trên địa bàn Hà Nội sẽ xuất hiện thêm nhiều bình trà đã miễn phí như thế này.';
        $news->type = '2';
        $news->save();

        $news = new News;
        $news->title = 'Sức khỏe công nhân';
        $news->image = 'skcongnhan.jpg';
        $news->content = 'Giám đốc công ty ABC "Chúng tôi luôn chăm lo tới sức khỏe công nhân"
        Chủ tịch Hồ Chí Minh là người sáng lập và rèn luyện Đảng Cộng sản Việt Nam mà đội quân tiên phong của Đảng là giai cấp công nhân. Ngoài việc chú trọng nâng cao vai trò, nǎng lực và tính tiên phong của giai cấp công nhân Việt Nam, Người còn quan tâm đến việc chǎm lo, bảo vệ sức khoẻ cho đội ngũ công nhân và nhân dân lao động nước ta. Cho nên, kể từ khi miền Bắc nước ta bước vào thời kỳ cách mạng xã hội chủ nghĩa cho tới khi Bác đi xa, mặc dù bận trǎm công nghìn việc với cương vị là Chủ tịch nước, Bác Hồ vẫn dành nhiều thời gian đi thǎm hỏi và chỉ bảo cán bộ, công nhân ở các nhà máy, xí nghiệp. Trong các chuyến thǎm hỏi đó, Bác đặc biệt chú ý đến vấn đề vệ sinh, bảo vệ sức khoẻ cho công nhân. Sự chǎm lo đó đi từ những sinh hoạt bình thường nhất có thể ảnh hưởng tới đời sống và sức khoẻ của công nhân. Bác quan tâm từ bếp ǎn, cống rãnh, nhà tiêu... của tập thể cũng như các gia đình công nhân. Vì người nghĩ rằng: "Nếu đời sống được cải thiện và mọi người được khoẻ mạnh thì sẽ phấn khởi thi đua sản xuất...". Đi tới đâu Bác cũng cǎn dặn: "... cán bộ phụ trách phải luôn luôn chú ý sǎn sóc đến đời sống của công nhân". Bác ân cần chǎm lo đời sống và sức khoẻ của công nhân bằng một tấm lòng của một người cha già nhân hậu đối với con cháu trong nhà... Theo ý Bác, việc giữ gìn, bảo vệ sức khoẻ không chỉ đơn thuần là công việc của các thầy thuốc, mà là trách nhiệm chung của tất cả mọi người. Mặt khác, muốn có sức khoẻ tốt cũng không phải chỉ giữ gìn vệ sinh tốt là đủ, mà còn phải tǎng cường rèn luyện thân thể, sinh hoạt điều độ và phải bảo đảm an toàn trong lao động. Ngày 25-12-1958, khi đến thǎm Nhà máy cơ khí Hà Nội, Bác đã cǎn dặn mọi người phải đoàn kết, tôn trọng kỷ luật lao động, chống làm bừa làm ẩu. Người nhấn mạnh: phòng bệnh hơn chữa bệnh. Khi coi người lao động là vốn quý nhất thì việc chǎm lo bảo vệ sức khoẻ cho con người lao động chính là nhằm bảo vệ cái "vốn quý nhất" đó. ở đây, lời khuyên của Bác không chỉ là sự quan tâm thông thường mà nó mang đậm tính nhân vǎn sâu sắc của một tấm lòng, một con người suốt đời phấn đấu vì nước vì dân. Tính nhân vǎn đó là cội nguồn, là sức sống của một tấm lòng nhân hậu còn sống mãi trong lòng mọi người. Trong khi Bác động viên mọi người hǎng hái, tích cực lao động sản xuất, Bác không chỉ đơn giản nghĩ tới kết quả lao động mà lao động nhằm cải thiện và nâng cao đời sống của người công nhân và nhân dân lao động. Đó mới chính là mục đích của lao động. Xét đến cùng, đây cũng là một quan niệm giữ gìn và bảo vệ sức khoẻ cho con người lao động. Xem xét ở góc độ này, chúng ta mới càng nhận thấy sự vĩ đại nhưng vô cùng giản dị và nhân hậu của tấm lòng Bác Hồ. Trong lời phát biểu ở Trường cán bộ công đoàn, Bác đã nhấn mạnh: "Mục đích của công đoàn là phải cải thiện đời sống công nhân...". Như vậy, theo quan niệm của Bác, để bảo vệ sức khoẻ của công nhân và người lao động cần chú ý tới nhiều yếu tố, trong đó việc cải thiện đời sống người lao động là một yếu tố quan trọng. Bởi vì, khi đời sống của công nhân và người lao động được cải thiện cũng có nghĩa là đã góp phần bảo vệ và tǎng cường sức khoẻ cho người lao động. Việc tìm mọi biện pháp để bồi dưỡng, tǎng thể lực cho người lao động mới thực sự là cách chǎm lo thiết thực, đầy nhân ái của Bác. Chữa bệnh là một biện pháp bảo vệ sức khoẻ, song đó chỉ là một biện pháp đối phó, bị động, "cực chẳng đã", còn phòng bệnh mới thực là một giải pháp tốt nhằm bảo vệ sức khoẻ cho con người. Một trong những biện pháp phòng bệnh có hiệu quả là cải thiện đời sống cho người lao động. Ngoài ra giải pháp phòng bệnh không kém phần quan trọng là việc giữ gìn vệ sinh sạch sẽ để tạo ra môi trường sống trong lành. Cho nên, đến thǎm và làm việc với bất cứ nhà máy nào, xí nghiệp nào, Bác cũng đều quan tâm và khuyên rǎn mọi người phải giữ gìn vệ sinh, bảo vệ môi trường trong sạch. Hình ảnh Bác ân cần khuyên nhủ cán bộ, công nhân Nhà máy cơ khí Trần Hưng Đạo (1961) khi Bác đứng bên rãnh thoát nước của bếp ǎn tập thể nhà máy là hình ảnh vừa vĩ đại, vừa cảm động. Như vậy, theo quan niệm của Bác, việc giữ gìn sức khoẻ không chỉ đơn thuần là công sức của các thầy thuốc, không chỉ riêng ngành y tế quan tâm mà đây là trách nhiệm chung của mọi người, của toàn xã hội. Theo Bác, việc bảo vệ giữ gìn sức khoẻ cho người lao động, cho giai cấp công nhân không chỉ là trách nhiệm chung của tất cả mọi người, mà còn là mục tiêu, là trách nhiệm to lớn của Đảng và Nhà nước ta. Bác Hồ là lãnh tụ, song người không chỉ dành riêng tâm trí cho những chiến lược to lớn của đất nước mà Người luôn dành sự quan tâm chǎm lo đến những sinh hoạt rất đời thường của đội ngũ công nhân và nhân dân lao động. Thực ra, những chiến lược to lớn mà Bác đề ra cũng không ngoài mục đích làm cho dân giàu nước mạnh, mọi người được ấm no, hạnh phúc.';
        $news->type = '3';
        $news->save();

        $news = new News;
        $news->title = 'ABC Kitchen - Cam kết đảm bảo vệ sinh an toàn thực phẩm';
        $news->image = 'vsantoantp.jpg';
        $news->content = 'Việt Tín có dich vụ tư vấn xin giấy phép vệ sinh thực phẩm cho khách hàng đang vướng mắc các thủ tục hồ sơ pháp lý. Có rất nhiều ngành nghề kinh doanh, buôn bán và sản xuất liên quan đến thực phẩm, thực phẩm chức năng cần phải có giấy phép vệ sinh an toàn thực phẩm, nhưng không hẳn ai cũng rành về thủ tục hồ sơ xin giấy phép.
        Việt Tín chuyên tư vấn xin giấy phép vệ sinh, lập hồ sơ và hướng dẫn tham gia lớp tập huấn vệ sinh an toàn thực phẩm, nếu bạn đang vướng mắc các hồ sơ thủ tục hãy liên hệ ngay tới Việt Tín để được tư vấn trực tiếp …
        Tổ chức, hộ gia đình, cá nhân Việt Nam, tổ chức, cá nhân nước ngoài sản xuất, kinh doanh thực phẩm trên lãnh thổ Việt Nam phải tuân theo các quy định của Pháp lệnh an toàn thực phẩm và các quy định khác của pháp luật có liên quan. Trường hợp điều ước quốc tế mà Cộng hòa xã hội chủ nghĩa Việt Nam ký kết hoặc gia nhập có quy định khác với quy định của Pháp lệnh an toàn thực phẩm thì áp dụng điều ước quốc tế đó.                    ';
        $news->type = '3';
        $news->save();

        $news = new News;
        $news->title = 'Không gian nhà ăn thoáng mát - sạch đẹp';
        $news->image = 'khonggian.jpg';
        $news->content = 'Từ những trang trí hoa văn tinh tế trên tường, trần nhà hàng đến thiết kế quầy bar, bếp lịch sự, Sushi Kei thực sự là một Nhật Bản thu nhỏ giữa lòng Hà Nội.
        Trang trí tường cũng như bàn ghế với màu cam và xanh lá cây chủ đạo tạo sự gần gũi với thiên nhiên như trong văn hóa Nhật Bản
        Nhà hàng Nhật Bản Sushi Kei hứa hẹn là một trong những điểm đến lý tưởng cho thực khách muốn trải nghiệm và yêu ẩm thực Nhật Bản.';
        $news->type = '3';
        $news->save();

        $news = new News;
        $news->title = 'Thực phẩm tươi sạch';
        $news->image = 'thucphamsach.jpg';
        $news->content = 'Thực phẩm bẩn hiện nay đang trở thành nỗi lo thường trực của nhiều bà nội trợ. Thông tin liên tiếp về thực phẩm nhiễm khuẩn, không đạt chất lượng an toàn vệ sinh thực phẩm đã gây tâm lý hoang mang tới người tiêu dùng. Dường như chất hóa học độc hại có trong hầu hết các loại thực phẩm, từ tôm cá, rau củ đến các loại hoa quả, bánh trái, từ đồ ăn tươi sống đến thực phẩm khô, gia vị,… gây ảnh hưởng xấu và lâu dài đến sức khỏe của con người. Nắm bắt được tâm lý cùng với nhu cầu tăng cao của người tiêu dùng, nhiều cửa hàng kinh doanh thực phẩm sạch ra đời và ngày càng phát triển mạnh mẽ. Song không phải bất cứ ai kinh doanh lĩnh vực này cũng thành công. Chị Trương Thị Hiền, chủ cửa hàng thực phẩm sạch trên phố Đội Cấn chia sẻ: “10 cửa hàng thực phẩm sạch mở ra thì có tới hơn một nửa phải đóng cửa. Nguyên nhân là do thói quen của người dân vẫn thường mua thực phẩm tại chợ. Giá cả thực phẩm tại cửa hàng cao, không cạnh tranh được với thực phẩm trôi nổi. Tuy nhiên với số vốn từ 50 đến 300 triệu đầu tư mở một cửa hàng thực phẩm sạch bạn vẫn nắm chắc được sự thành công, quan trọng là bạn phải có được nguồn hàng ổn định, chất lượng tốt, và biết cách bảo quản thực phẩm”.';
        $news->type = '3';
        $news->save();

    }

}
