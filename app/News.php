<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class News extends Model  {
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'newss';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['title','content'];
}
