<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', 'PageController@home');
Route::get('home', 'PageController@home');
Route::get('about', 'PageController@about');
Route::get('sitemap', 'PageController@sitemap');
Route::get('contact', 'PageController@contact');
Route::get('news', 'PageController@news');
Route::post('resignfood', 'DishController@resign');
Route::post('cancelresignfood', 'DishController@destroyresign');
Route::get('news/{id}', 'NewsController@show');





Route::group(['middleware' => 'auth'], function() {
	Route::get('/resignfood', 'MenuController@show');
	Route::get('/resignfood/{eat_time}', 'MenuController@showfordate');
	Route::get('menus/{eat_time}/data', 'Admin\MenuController@getmenu');
	Route::get('users/{username}', 'UserController@index');
	Route::get('users/{username}/{date}/data', 'UserController@paymentdata');
});


Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

if (Request::is('admin/*'))
{
    require __DIR__.'/admin_routes.php';
}