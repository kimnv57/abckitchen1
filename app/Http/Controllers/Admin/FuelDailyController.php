<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\FuelDaily;
use App\Http\Requests\Admin\FuelDailyRequest;
use App\Http\Requests\Admin\FuelDailyEditRequest;
use App\Http\Requests\Admin\DeleteRequest;
use Illuminate\Support\Facades\Input;
use Datatables;

class FuelDailyController extends AdminController {

    /*
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function index()
    {
        // Show the page
        return view('admin.fueldailys.index');
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate() {
        return view('admin.fueldailys.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postCreate(FuelDailyRequest $request) {

        $fueldaily = new FuelDaily();
        $fueldaily -> name = $request->name;
        $fueldaily -> price = $request->price;
        $fueldaily->save();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $fueldaily
     * @return Response
     */
    public function getEdit($id) {

        $fueldaily = FuelDaily::find($id);

        return view('admin.fueldailys.edit', compact('fueldaily'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $fueldaily
     * @return Response
     */
    public function postEdit(fueldailyEditRequest $request, $id) {

        $fueldaily = fueldaily::find($id);
        $fueldaily -> price = $request->price;
        $fueldaily->save();
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param $fueldaily
     * @return Response
     */

    public function getDelete($id)
    {
        $fueldaily = fueldaily::find($id);
        // Show the page
        return view('admin.fueldailys.delete', compact('fueldaily'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $fueldaily
     * @return Response
     */
    public function postDelete(DeleteRequest $request,$id)
    {
        $fueldaily= fueldaily::find($id);
        $fueldaily->delete();
    }

    /**
     * Show a list of all the languages posts formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function data()
    {
       
    $fueldailys = fueldaily::select(array('fueldailys.id','fueldailys.id','fueldailys.name','fueldailys.price', 'fueldailys.created_at'))->orderBy('fueldailys.name', 'ASC');

        return Datatables::of($fueldailys)
            ->add_column('actions', '<a href="{{{ URL::to(\'admin/fueldailys/\' . $id . \'/edit\' ) }}}" class="btn btn-success btn-sm iframe" ><span class="glyphicon glyphicon-pencil"></span>  {{ Lang::get("admin/modal.edit") }}</a>
                    <a href="{{{ URL::to(\'admin/fueldailys/\' . $id . \'/delete\' ) }}}" class="btn btn-sm btn-danger iframe"><span class="glyphicon glyphicon-trash"></span> {{ Lang::get("admin/modal.delete") }}</a>
                ')
            ->add_column('check','<input class="mycheckbox" type="checkbox" name="check[]" value="{{$id}}"/>')
            ->make(true);
    }

    public function show($id)
    {
        $fueldaily = fueldaily::find($id);
        // Show the page
        return view('fueldailys.index', compact('fueldaily'));
    }

   
}
