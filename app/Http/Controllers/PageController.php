<?php namespace App\Http\Controllers;
use App\News;

class PageController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function about()
	{
		return view('pages.about');
	}
	public function contact()
	{
		return view('pages.contact');
	}
	public function sitemap()
	{
		return view('pages.sitemap');
	}
	public function news()
	{
		$news=News::all();
		return view('pages.news',compact('news'));
	}

	public function home()
	{
		$nhanvien = News::where("type","=",1)->orderBy('created_at', 'ASC')->first();
		$uudai = News::where("type","=",2)->orderBy('created_at', 'ASC')->first();
		$thucpham = News::where("type","=",3)->orderBy('created_at', 'ASC')->first();
		return view('pages.home',compact('nhanvien','uudai','thucpham'));
	}

}
