<?php namespace App\Http\Controllers;
use Auth;
use App\Menu;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use App\User_Menu;
class MenuController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function showfordate($date)
	{
		
		
		return view('dishes.showmenu',compact('date','resign'));
	}

	public function show()
	{
		$resign=false;
		$ldate = date('Y-m-d');
		$menus = Menu::where("eat_time","like",$ldate);
		if($menus->count()!=0) {
			$users = User_Menu::where("user_id","=",Auth::user()->id)->where("menu_id","=",$menus->first()->id);

		}
		
		$date='';
		return view('dishes.showmenu',compact('date','resign'));
	}

}
